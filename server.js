const express = require ("express");
const app = express();
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const DATABASE_NAME = 'productsdb';
const MONGO_URL = `mongodb://localhost:27017`;

let db = null;
let products = null;
let orders = [];
let order;
let orderRowId=0;
let orderId=0;
let ordersCollection =null;
app.set('view engine', 'ejs');
app.use(express.static("public"))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

async function onGetRoot(req,res) {
    findMaxId();

    res.render("index", {orders});
}

function findMaxId(){
    for(order of orders){
        if(order.id>orderId){
            orderId=order.id;
        }
    }
}
async function showCurrentOrder(req, res){
    res.render('order-new',{order});
}

async function saveProduct(req, res) {
    const clientName = req.body.clientName;
    const productName = req.body.productName;
    const quantity = req.body.quantity;

    order.Clientname=clientName;
    order.amount = order.amount + 1;
    
    orderRowId++;
    let row=new Row(productName, quantity,orderRowId++);
    order.addOrder(row);
    res.redirect('/order-show');
}
async function showOrder(req, res) {
    let cursor = await products.find()
                                   .project({ name: 1 })
                                   .sort({name: 1});
    const arrayOfProducts = await cursor.toArray();
    const productsNames = arrayOfProducts.map( o => o.name );
    res.render("new-order", {order, productsNames});
}
async function toCompleteOrder(requestedOrder){
    await { then: resolve => setTimeout(() => resolve(), 120000) };
    requestedOrder.state=3;
}
async function saveAsRequested(req, res) {
    toCompleteOrder(order);
    order.state=2;
    order.date=now();
    saveOrderLogic(order);
    res.redirect('/');
}
async function saveOrderLogic(req, res) {
    if(toInsert()){
        orderId++;
        order.id=orderId;
        orders.push(order);
        saveOrder(order);
    }
    else{
        updateOrder();
    }
    res.redirect('/');
}
async function removeProduct(req, res){
    let rowId=req.params.id;
    order.amount = order.amount - 1;
    removeProductFromToOrder(rowId);
    res.redirect(`/order-show`);
}
function removeProductFromToOrder(productId){
    let index=findIndex(productId);
    order.rows.splice(index, 1);
}
function findIndex(productId){
    let orderRows = order.rows;
    for(let i=0; i<orderRows.length; i++){
        if(orderRows.id===productId){
            return i;
        }
    }
}

app.get("/", onGetRoot);

app.get("/order-new",onNewOrder);
app.get("/order-show", showOrder);
app.get("/order-show-details", showOrderDetails)
app.get("/order-edit/:orderId", showOrderById);
app.get("/product-remove/:idProduct", removeProduct);
app.get("/order-request-show/:orderId", showOrderRequested);
app.post("/product-add",saveProduct);
app.post("/order-save",saveOrderLogic);
app.post("/send-to-requested", saveAsRequested);

async function showOrderDetails(req, res) {
    let id=req.params.orderId;
    findOrderById(id);

    res.render("order-requested-details", {order});
}
async function showOrderRequested(req, res) {
    let id=req.params.orderId;
    findOrderById(id);

    res.redirect(`/order-show-details`);
}

async function showOrderById(req, res) {
    let id=req.params.orderId;
    findOrderById(id);
    res.redirect(`/order-show`);
}
function findOrderById(id){
    for(soughtOrder of orders){
        
        if(soughtOrder.id+""===id+""){
            order=soughtOrder;
        }
    }
}

async function onNewOrder(req, res) {
    order=new Order("","");
    let cursor = await products.find()
                                   .project({ name: 1 })
                                   .sort({name: 1});
    const arrayOfProducts = await cursor.toArray();
    const productsNames = arrayOfProducts.map( o => o.name );
    res.render('new-order',{order, productsNames});
}

async function startServer() {
    const client = await MongoClient.connect(MONGO_URL, { useNewUrlParser: true });
    db = client.db(DATABASE_NAME);
    products = db.collection('products');
    ordersCollection = db.collection('orders');
    
    app.listen(3000);
    console.log("Server ready on port 3000");
}


async function updateOrder() {
    updateOrdersArray();
    updateDB();
}
async function updateDB(){
    let id=order.id;
    ordersCollection.remove(
        { id: id }
    )
    saveOrder(order);
}

async function updateOrdersArray(){
    let orderId = order.id;
    removeOrderFromOrders(orderId);
    orders.push(order);
}

function removeOrderFromOrders(orderId){
    let index=findIndexOrder(orderId);
    orders.splice(index, 1);
}

function findIndexOrder(orderId){
    for(let i=0; i<orders.length; i++){
        if(orders[i].id===orderId){
            return i;
        }
    }
}

async function saveOrder(order){
    ordersCollection.insertOne(order);
}

function toInsert(){
    return order.id==="";
}
startServer();


class Order{
	constructor(name, id){

		this.Clientname=name;
        this.rows=[];
        this.state=1;
        this.id=id;
        this.date=now();
        this.amount=0;
    }
    
    clone(item){

		this.Clientname=item.Clientname;
        this.rows=item.rows;
        this.state=item.state;
        this.id=item.id;
        this.date=now();
    }

	addOrder(row){
		this.rows.push(row);
    }
}

class Row{
    constructor(nameOfProduct, quantity,id){
        this.nameOfProduct=nameOfProduct;
        this.quantity=quantity;
        this.id=id;
    }
}

function now(){
    let date = new Date();
    let today = date.getDate();
    let month = date.getMonth()-1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();

    let complete = today+"/"+month+"/"+year+" "+hour+":"+minutes;
    return complete;
}