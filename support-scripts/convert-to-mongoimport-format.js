const fse = require('fs-extra');
const products = require('./products.json');

// This reformats "dictionary.json" into a JSON file that can be loaded into
// MongoDB via the `mongoimport` command, formatted in the way the dictionary
// examples expect.
(async function() {
  let formatted = '';
  for (const product of products) {
    formatted += JSON.stringify(product);
  }
  await fse.writeFile('./formatted-products.json', formatted);
})();