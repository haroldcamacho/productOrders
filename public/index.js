function changeTab(event) {
    if(event.target.dataset.id) {
        const pressed = event.target;
        const selected = document.querySelector(".selectedButtonOption");
        const selectedId = selected.dataset["id"];
        document.getElementById(selectedId).classList.add("hidden");
        selected.classList.remove("selectedButtonOption");
        pressed.classList.add("selectedButtonOption");
        const newSelectedId = pressed.dataset.id;
        document.getElementById(newSelectedId).classList.remove("hidden");
    }
}

function eventHandlers() {
    const chooserTab = document.querySelector("#tabs-container ul");
    chooserTab.addEventListener("click", changeTab);
}

eventHandlers();